import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    body_py = json.loads(body)
    p_name = body_py["presenter_name"]
    p_title = body_py["title"]
    p_email = body_py["presenter_email"]
    send_mail(
        "Your presentation has been accepted",
        f"{p_name}, we're happy to tell you that your presentation {p_title} has been accepted.",
        "admin@conference-go",
        [p_email],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    body_py = json.loads(body)
    name = body_py["presenter_name"]
    title = body_py["title"]
    email = body_py["presenter_email"]
    send_mail(
        "Your presentation has been rejected",
        f"{name}, we're sorry to inform you that your presentation {title} has been rejected. Please try again.",
        "admin@conference-go",
        [email],
        fail_silently=False,
    )


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
