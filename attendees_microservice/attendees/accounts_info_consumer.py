from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO
from datetime import datetime

# Declare a function to update the AccountVO object (ch, method, properties, body)
def update_account_vo(ch, method, properties, body):
    #   content = load the json in body
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = datetime.isoformat(updated_string)
    if is_active:
        #       Use the update_or_create method of the AccountVO.objects QuerySet
        account = AccountVO.objects.update_or_create(
            first_name=first_name,
            last_name=last_name,
            email=email,
            updated=updated,
        )
        account.save()
    #           to update or create the AccountVO object
    #   otherwise:
    else:
        account = AccountVO.objects.filter(email=email)
        account.delete()


#       Delete the AccountVO object with the specified email, if it exists


# Based on the reference code at
#   https://github.com/rabbitmq/rabbitmq-tutorials/blob/master/python/receive_logs.py
# infinite loop
while True:
    #   try
    try:
        #       create the pika connection parameters
        parameters = pika.ConnectionParameters(host="rabbitmq")
        #       create a blocking connection with the parameters
        connection = pika.BlockingConnection(parameters)
        #       open a channel
        channel = connection.channel()
        #       declare a fanout exchange named "account_info"
        channel.exchange_declare(
            exchange="account_info", exchange_type="fanout"
        )
        result = channel.queue_declare(queue="")
        #       declare a randomly-named queue
        #       get the queue name of the randomly-named queue
        queue_name = result.method.queue
        #       bind the queue to the "account_info" exchange
        channel.queue_bind(exchange="account_info", queue=queue_name)
        #       do a basic_consume for the queue name that calls
        #           function above
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=update_account_vo,
            auto_ack=True,
        )
        #       tell the channel to start consuming
        channel.start_consuming()
    except AMQPConnectionError:
        #       print that it could not connect to RabbitMQ
        print("Could not connect to RabbitMQ")
        #       have it sleep for a couple of seconds
        time.sleep(2.0)
