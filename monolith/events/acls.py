# outside server testing
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

# VSCode testing
# from keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

import requests
import json


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    query = {"query": f"{city}, {state}"}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=query, headers=headers)
    content = response.json()
    try:
        picture_url = content["photos"][0]["src"]["original"]
        return {"picture_url": picture_url}
    except (KeyError, IndexError):
        return {"picture_url": None}


def kelvin_to_farenheit(temp):
    return (temp - 273.15) * 9 / 5 + 32


def get_weather_data(city, state):
    # finding the latitude and longitude of a city
    location_url = "http://api.openweathermap.org/geo/1.0/direct"
    query = {"q": [city, state], "appid": OPEN_WEATHER_API_KEY}
    location_response = requests.get(location_url, params=query)
    location_content = location_response.json()
    lat = location_content[0]["lat"]
    lon = location_content[0]["lon"]

    # finding the temp in farenheit and a description
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_query = {"lat": lat, "lon": lon, "appid": OPEN_WEATHER_API_KEY}
    weather_response = requests.get(weather_url, params=weather_query)
    weather_content = weather_response.json()
    description = weather_content["weather"][0]["description"]
    temp = kelvin_to_farenheit(weather_content["main"]["temp"])
    return {"weather": {"description": description, "temp": temp}}


# get_photo("Philadelphia", "PA")
# get_weather_data("Philadelphia", "PA")
# http://api.openweathermap.org/geo/1.0/direct?q={city name},{state code},{country code}&limit={limit}&appid={API key}
# https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}
