# Generated by Django 4.0.3 on 2023-05-10 20:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('presentations', '0002_alter_status_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='name',
            field=models.CharField(max_length=10),
        ),
    ]
